import 'package:flutter/material.dart';

const cBackgroundColor = Color(0xFFF6F6F6);
const cPrimaryColor = Color(0xFF517078);
const cSecondaryColor = Color(0xFFC5D2D6);
const cTextColor = Color(0xFF1F2537);
const cSubtleTextColor = Color(0xB31F2537);
const cShadowColor = Color(0xFFE6E6E6);