
// Home screen
const sCatalogTitle = "Catalog";
const sFoundResults = "Found 10 Results";

// Product screen
const sProductDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu eros blandit, gravida arcu tristique, sollicitudin sem.";
const sRelatedTitle = "Related products";

// About screen
const sAboutTitle = "About";
const sIllustrationCredit1 = "Illustrations by ";
const sIllustrationCredit2 = "Icons 8 ";
const sIllustrationCredit3 = "from ";
const sIllustrationCredit4 = "Ouch!";