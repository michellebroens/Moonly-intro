import 'package:flutter/material.dart';
import 'package:intro_app/constants/colors.dart';
import 'package:intro_app/features/about/about.dart';
import 'package:intro_app/features/home/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Moonly Intro',
      theme: ThemeData(
        fontFamily: "Poppins",
        scaffoldBackgroundColor: cBackgroundColor,
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 34.0, fontWeight: FontWeight.w600, color: cTextColor),
          headline2: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, color: cPrimaryColor),
          subtitle1: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600, color: cTextColor),
          subtitle2: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600, color: cTextColor),
          bodyText1: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400, color: cTextColor),
          bodyText2: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400, color: cPrimaryColor),
          caption: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400, color: cSubtleTextColor),
          button: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600, color: Colors.white),
      ),
      ),
      home: const HomeScreen(),
    );
  }
}
