import 'package:flutter/material.dart';

class Item {
  final String title;
  final String price;
  final String image;

  const Item({
    required this.title,
    required this.price,
    required this.image, 
  });
}