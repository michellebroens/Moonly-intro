import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:intro_app/constants/colors.dart';
import 'package:intro_app/features/product/product.dart';

class ItemCard extends StatelessWidget {
  final String title;
  final String price;
  final String image;

  const ItemCard({
    Key? key, 
    required this.title, 
    required this.price, 
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(25),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(25),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 17),
              blurRadius: 17,
              spreadRadius: -23,
              color: cShadowColor
            )
          ]
        ),
        child: Material(
          color: Colors.transparent,
          child: GestureDetector(
            onTap: ()  {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ProductScreen(
                  title: title,
                  price: price, 
                  image: image
                )),
              );
            },
             child: InkWell(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                child: Column(
                  children: <Widget>[
                    Spacer(),
                    Image.asset(image),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        title, 
                        style: Theme.of(context).textTheme.subtitle2,
                        textAlign: TextAlign.left
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                      price,
                      style: Theme.of(context).textTheme.caption,
                      textAlign: TextAlign.left
                      ),
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
          ),
         
        )
      ),
    );
  }
}